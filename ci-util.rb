require 'yaml'
require 'net/https'
require 'uri'

# Strip refs/heads/ from branch names
def simple_branch_name(ref)
    ref.sub("refs/heads/", "")
end

# Parse configuration from ci-config.yml and return the specified child config map
def read_config(childKey, req_keys = [])
    hookDir = File.expand_path File.dirname(__FILE__)
    configPath = hookDir + "/ci-config.yml"
    raise "No ci-config.yml found." unless File.exists? configPath
    config = YAML.load_file(configPath)
    raise "ci-config.yml file is incomplete: missing '#{child}'" unless config[childKey]
    child = config[childKey]
    req_keys.each { |key|
        raise "ci-config.yml file is incomplete: '#{key}' is required for #{childKey}" unless child[key]
    }
    child
end

def exit_if_not_protected_ref(ref)
    # if protected_refs is defined, only check build status for specified refs
    protected_refs = read_config("protected_refs")    

    if protected_refs and not protected_refs.include?(ref)
        exit # ref is not protected   
    end 
end

# Grab the first non-merge commit in the new tip's history that doesn't appear in the old 
# tip. This will be the old tip of the feature branch most recently merged into master, 
# provided the developer hasn't been commiting directly to master. If they have (naughty 
# dev!), they will need to get that commit built by Bamboo to pass this check. Pushing a 
# new branch pointing at that commit should be enough to get it built, provided Bamboo is 
# configured to build branches.
def find_newest_non_merge_commit(prevCommit, newCommit)
    commit = `git log -1 --no-merges --format="%H" #{prevCommit}..#{newCommit}`.chomp
    if commit.empty?
        exit # update doesn't introduce any new commits (e.g. a forced update to an earlier commit)
    end    
    commit
end

# Concatenate the server's base URL with the specified path and return a URI
def createUri(server_config, path)
    baseUrl = server_config["url"]
    # assume https if no scheme spcified
    if not baseUrl.start_with? "http"
        baseUrl = "https://#{baseUrl}"
    end
    # strip trailing slashes
    while baseUrl.end_with? "/"
        baseUrl = baseUrl[0..-2]
    end
    URI.parse("#{baseUrl}#{path}")
end

# HTTP GET the specified resource and return the response
def httpGet(server_config, path, failIfNotOk = true)
    uri = createUri(server_config, path)
    req = Net::HTTP::Get.new(uri.to_s)
    httpAuthAndExec(server_config, req, uri, failIfNotOk)
end 

# HTTP POST the provided request body to the specified resource and return the response
def httpPost(server_config, path, body, content_type, failIfNotOk = true)
    uri = createUri(server_config, path)
    req = Net::HTTP::Post.new(uri.to_s, initheader = {'Content-Type' => content_type})
    req.body = body
    httpAuthAndExec(server_config,req,uri, failIfNotOk)    
end

# Authenticate and execute the supplied request and return the response if successful
def httpAuthAndExec(server_config, req, uri, failIfNotOk)
    req.basic_auth server_config['username'], server_config['password']
    http = Net::HTTP.new(uri.host, uri.port)
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE 
    http.use_ssl = uri.scheme.eql?("https")    
    response = http.start {|http| http.request(req)}
    if failIfNotOk
        raise "Request failed: #{response.code} from server" unless response.is_a? Net::HTTPOK
    end
    response
end

# Look up a build result specified by plan key for a particular commit, and download the
# specified artifact.
def shared_artifact_for_commit(bamboo, commit, plan_key, artifact_path)
    # query bamboo for build results
    response = httpGet(bamboo, "/rest/api/latest/result/byChangeset/#{commit}")
    # find build key
    doc = Document.new response.body

    # look up the coverage plan from the build results for the merged branch
    build_key = XPath.first(doc, "//plan[@shortKey='#{plan_key}']/../@key")
    if build_key == nil
        puts "The #{plan_key} build has not run for #{shortSha(commit)}"
        exit 1
    end    

    # grab the specified shared artifact from the build
    response = httpGet(bamboo, "/browse/#{build_key}/artifact/shared/#{artifact_path}")
    raise "Request failed: #{response.code} from server" unless response.is_a? Net::HTTPOK
    response.body
end

# Util function for pluralizing quantities
def pluralize(count, single, multiple)
    count == 1 ? single : multiple
end

# Shorten commit SHA for display 
def shortSha(sha)
    sha[0..7]
end
